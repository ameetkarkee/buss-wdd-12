<?php
	include("auth_session.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>HOMEPAGE</title>
</head>
<body>
	<h1>THIS IS HOME PAGE.</h1>
		<p>Hey, <?php echo $_SESSION['email']; ?>!</p>
        <p>You are now user dashboard page.</p>
        <p><a href="logout.php">Logout</a></p>
        <!-- table create using databse data -->
        <table>
        	<tr>
        		<th>Student Id</th>
        		<th>Student Name</th>
        		<th>Student Address</th>
        		<th>Email</th>
        	</tr>
        	<?php
        		include_once 'dbconect.php';

        		$sql = "SELECT sid, sname, address, email from student";
        		$result= $conn-> query($sql);

        		if($result->num_rows>0){
        			while ($row =  $result->fetch_assoc()) {
		        		echo "<tr>
				        		<td>".$row["sid"]."</td>
				        		<td>".$row["sname"]."</td>
				        		<td>".$row["address"]."</td>
				        		<td>".$row["email"]."</td>
		        			</tr>";
        			}
        		}
        		echo "</table>";

        		$conn->close();

        	?>      
</body>
</html>