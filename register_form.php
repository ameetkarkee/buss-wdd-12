<!DOCTYPE html>
<html>
<head>
	<title>REGISTERATION</title>
	<link rel="stylesheet" type="text/css" href="registration_style.css">
</head>
<body>

	<div class="reg-form">
		
		<form action="register_process.php" method="POST">

			<div>
				<h3>REGISTRATION FORM</h3>
			</div>
		
			<div class="txtb">
				<label>Student Name:</label>
				<input type="text" name="sname" placeholder="Enter the student name">
			</div>
			<div class="txtb">
				<label>Address:</label>
				<input type="text" name="saddress" placeholder="Enter the student address">
			</div>
			
			<div class="txtb">
				<label>Email:</label>
				<input type="text" name="semail" placeholder="Enter the student email">
			</div>
			
			<div class="txtb">
				<label>Password:</label>
				<input type="password" name="spassword" placeholder="Enter the password">
			</div>
			
			<div class="txtb">
				<input class="btn" type="submit" name="submit" value="SUBMIT">
			</div>
			
		</form>
	</div>
</body>
</html>